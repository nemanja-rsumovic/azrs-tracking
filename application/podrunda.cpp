#include "podrunda.h"
#include <QTimer>

Podrunda::Podrunda ()
{
  void setAnswerQuestion (QString pitanje, double odgovor);

}

std::pair<QString, double>
Podrunda::getAnswerQuestion () const
{
  return answerQuestion;
}

void
Podrunda::setAnswerQuestion (QString pitanje, double odgovor)
{
  answerQuestion.first = pitanje;
  answerQuestion.second = odgovor;
}
